#!/bin/bash
#clear
source ./vultr-script-config.sh
echo -e "[46m[30mWelcome to Cloud VM Control Panel[0m"
echo -e "[41m[256mDeveloped by Cyber Wings[0m"
echo "Version: 1.0"
echo
echo -e "This script can be used with [46m[30mVultr[0m"
echo -e "[100m[256mhelp[0m can be used to check usage."

while read -p "[46m[30mVultrVM-Control>[0m " input
do
	case $input in
		"list") echo -e "[104m[256mLIST OF THE VM IN YOUR ACCOUNT[0m"
			curl -sH "API-Key: $API_KEY" https://api.vultr.com/v1/server/list | sed 's/,/\n/g; s/{/\n/g' | grep "label\|\"main_ip\|SUBID" | sed 's/main_ip/>>> Main IP/g; s/label/>> Name/g; s/SUBID/>> SUBID/g; s/"//g; s/:/: /g' | awk -v n=3 '1; NR % n == 0 {print ""}'
			;;
		"login") source ./vultr-connect.sh
			;;
		"info") source ./vultr-info.sh
			;;
		"start") source ./vultr-start.sh
			;;
		"stop") source ./vultr-stop.sh
			;;
		"bill") source ./vultr-billing.sh
			;;
		"help") source ./help.sh
			;;
		"about") source ./about.sh
			;;
		"exit") exit
	esac
done
