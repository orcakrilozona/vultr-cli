#!/bin/bash

echo "[100m[256mhelp[0m	- Prints this message"
echo "[100m[256mabout[0m	- About the script"
echo "[100m[256mlist[0m	- List all the VM"
echo "[100m[256mlogin[0m	- Login to the VM"
echo "[100m[256mstart[0m	- Starts a VM"
echo "[100m[256mstop[0m	- Stops a VM"
echo "[100m[256minfo[0m	- Check information of VM"
echo "[100m[256mbill[0m	- Billing information of your account"
