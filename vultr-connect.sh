#!/bin/bash

select x in $(curl -sH "API-Key: $API_KEY" https://api.vultr.com/v1/server/list | sed 's/,/\n/g' | grep "\"main_ip" | sed 's/main_ip/Main IP/g; s/label/Name/g; s/"//g' | cut -d':' -f2) "exit"
do
	if [ "$x" = "exit" ]; then
		break
	fi

	read -p "Username: " usrname
	ssh $usrname@$x
done
