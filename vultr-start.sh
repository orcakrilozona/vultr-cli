#!/bin/bash

select x in $(curl -sH "API-Key: $API_KEY" https://api.vultr.com/v1/server/list | sed 's/,/\n/g' | grep "\"main_ip" | sed 's/main_ip/Main IP/g; s/label/Name/g; s/"//g' | cut -d':' -f2) "exit"
do
	if [ "$x" = "exit" ]; then
		break
	fi
	read -p "SUBID of the VM you want to start: " subid
	echo -e "[41m[256mWARNING! YOU ARE STARTING A VM ($x)[0m"
	read -p "Are you sure? (y/n) " sure
	if [ "$sure" = "y" ] || [ "$sure" = "Y" ]; then
		echo -e "[41m[256m$x is being STARTED[0m"
		curl -sH "API-Key: $API_KEY" --data "SUBID=$subid" https://api.vultr.com/v1/server/start
		echo "DONE!"
	else
		echo -e "SKIPPING"
	fi
done
