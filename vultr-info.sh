#!/bin/bash

select x in $(curl -sH "API-Key: $API_KEY" https://api.vultr.com/v1/server/list | sed 's/,/\n/g' | grep "\"main_ip" | sed 's/main_ip/Main IP/g; s/label/Name/g; s/"//g' | cut -d':' -f2) "exit"
do
	if [ "$x" = "exit" ]; then
		break
	fi
	curl -sH "API-Key: $API_KEY" -G --data "main_ip=$x" https://api.vultr.com/v1/server/list | sed 's/,/\n/g' | grep "\"main_ip\|label\|ram\|disk\|location\|current_bandwidth_gb\|allowed_bandwidth_gb\|power_status\|server_state" | sed 's/ram/RAM/g; s/disk/Storage/g; s/main_ip/Main IP/g; s/location/Location/g; s/current_bandwidth_gb/Current Bandwidth Used/g; s/allowed_bandwidth_gb/Allowed Bandwidth/g; s/power_status/Power Status/g; s/server_state/Server State/g; s/label/Name/g; s/"//g; s/:/: /g'
done
