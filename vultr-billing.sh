#!/bin/bash

echo -e "[104m[256mYOUR ACCOUNT BILLING STATUS[0m"
curl -sH "API-Key: $API_KEY" https://api.vultr.com/v1/account/info | sed 's/,/\n/g; s/{/\n/g; s/}/\n/g; s/"//g; s/balance/> Balance/g; s/pending_charges/> Pending Charges/g; s/last_payment_date/> Last Payment Date/g; s/last_payment_amount/> Last Payment Amount/g'
